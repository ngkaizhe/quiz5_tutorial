﻿using UnityEngine;
using System.Collections;

public class Magician : MonoBehaviour {
	public float x = 0;
	public float y = 2;
	public float z = 0;

	public Transform target;
	public GameObject obj;

	double timer=0.5;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.Rotate(x,y,z);

		if(timer<=0)
		{
			Instantiate(obj,target.position, Quaternion.identity);
			timer = 0.5;
		}
		else
		{
			timer -= Time.deltaTime;
		}
	}
}
