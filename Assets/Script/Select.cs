﻿using UnityEngine;
using System.Collections;

public class Select : MonoBehaviour {
	public GameObject Cube1;
	public GameObject Cube2;
	public GameObject Cube3;
	public GameObject Project1;
	public GameObject Project2;
	public GameObject Project3;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out hit))
			{
				if(hit.transform.name==Cube1.name)
				{
					Project1.SetActive(true);
					Project2.SetActive(false);
					Project3.SetActive(false);
				}
				else if(hit.transform.name==Cube2.name)
				{
					Project1.SetActive(false);
					Project2.SetActive(true);
					Project3.SetActive(false);
				}
				else if(hit.transform.name==Cube3.name)
				{
					Project1.SetActive(false);
					Project2.SetActive(false);
					Project3.SetActive(true);
				}
				else
				{
					Project1.SetActive(false);
					Project2.SetActive(false);
					Project3.SetActive(false);
				}
			}
		}
	}
}
